package com.example.demo.ctl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * TODO
 * className: HelloCtl
 * package: com.example.demo.ctl
 * author: cosco
 * mail: gt.fan@msn.cn
 * create 2018-12-08 1:28
 **/
@RestController
public class HelloCtl {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping("/hello")
    public String hello(){
        return "client - hello world !!! ";
    }

    @RequestMapping("/hello2")
    public String hello2(){
        return restTemplate.getForEntity("http://CONSUMER-SERVICE/hello2", String.class).getBody();
    }


}
