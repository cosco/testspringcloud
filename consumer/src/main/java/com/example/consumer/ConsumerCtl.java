package com.example.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * TODO
 * className: ConsumerCtl
 * package: com.example.consumer
 * author: cosco
 * mail: gt.fan@msn.cn
 * create 2018-12-08 1:36
 **/

@RestController
public class ConsumerCtl {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String hello(){
        return this.helloService.hello();
    }

    @RequestMapping("/hello2")
    public String hello2(){
        return "consumer - hello world!!!";
    }



}
